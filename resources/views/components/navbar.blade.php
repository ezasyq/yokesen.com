<header class="header header-home-one" style="position: relative">
    <nav class="navbar navbar-default header-navigation stricky">
        <div class="thm-container clearfix">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed bitmex-icon-menu" data-toggle="collapse" data-target=".main-navigation" aria-expanded="false"> </button>
                <a class="navbar-brand" href="index.html">
                    <img src="img/logo.png" alt="Awesome Image"/>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse main-navigation mainmenu " id="main-nav-bar">
                
                <ul class="nav navbar-nav navigation-box">
                    <li class="current"> 
                        <a href="{{ route('homePage') }}" >Home</a> 
                    </li>
                    <li> 
                        <a href="{{ route('aboutPage') }}">About</a> 
                    </li>
                    <li> 
                        <a href="services.html">Services</a> 
                        <ul class="sub-menu">
                            <li><a href="services.html">Service Page</a></li>
                            <li><a href="service-details.html">Service Details</a></li>
                        </ul><!-- /.sub-menu -->
                    </li>
                    <li> 
                        <a href="#">Pages</a> 
                        <ul class="sub-menu">
                            <li><a href="404.html">404 Page</a></li>
                            <li><a href="how-it-works.html">How it works</a></li>
                            <li>
                            	<a href="team.html">Our Team</a>
                            	<ul class="sub-menu">
                            		<li><a href="team.html">Team Style One</a></li>
                            		<li><a href="team2.html">Team Style Two</a></li>
                            		<li><a href="team3.html">Team Style Three</a></li>
                            	</ul><!-- /.sub-menu -->
                            </li>
                            <li><a href="what-is-bit.html">What Is Bitcoin</a></li>
                            <li><a href="exchange.html">Exchange</a></li>
                            <li><a href="charts.html">Charts</a></li>
                        </ul><!-- /.sub-menu -->
                    </li>
                    <li> 
                        <a href="blog.html">News</a> 
                        <ul class="sub-menu">
                            <li><a href="blog.html">News Grid</a></li>
                            <li><a href="blog-details.html">News Details</a></li>
                        </ul><!-- /.sub-menu -->
                    </li>
                    <li> <a href="contact.html">Contact</a> </li>
                </ul>                
            </div><!-- /.navbar-collapse -->
            <div class="right-side-box">
                <a href="#test-search" class="search-icon popup-with-zoom-anim bitmex-icon-search"></a><!--
                --><a href="#hidden-sidebar" class="side-menu-icon bitmex-icon-menu side-nav-opener"></a>
            </div><!-- /.right-side-box -->
        </div><!-- /.container -->
    </nav>   
</header><!-- /.header -->

<div class="search_area zoom-anim-dialog mfp-hide" id="test-search">
    <div class="search_box_inner">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="bitmex-icon-search"></i></button>
            </span>
        </div>
    </div>
</div>


<section class="hidden-sidebar side-navigation">
    <a href="#" class="close-button side-navigation-close-btn fa fa-times"></a><!-- /.close-button -->
    <div class="sidebar-content">
        <h3>Crypto Bitcoin <br /> Currency <br /> Html Template</h3> 
        <p>Lorem ipsum dolor sit amet adipiscing elitn quis ex et mauris vulputate semper Etiam eget lacus dapibs ultricies diam vel sollicitudin.</p>
        <p class="contact-info">Inquiry@bitcoin.com <br /> 2800 256 508</p><!-- /.contact-info -->
        <div class="social">
        	<a href="#" class="fa fa-twitter"></a><!--
        	--><a href="#" class="fa fa-facebook"></a><!--
        	--><a href="#" class="fa fa-youtube-play"></a><!--
        	--><a href="#" class="fa fa-pinterest"></a>
        </div><!-- /.social -->
    </div><!-- /.sidebar-content -->
</section><!-- /.hidden-sidebar -->