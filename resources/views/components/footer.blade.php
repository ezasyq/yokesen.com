<div class="footer-top">
	<div class="thm-container">
		<div class="row">
			<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
				<div class="single-footer-top">
					<p><i class="bitmex-icon-envelope"></i><span>Email: </span>contact@yokesen.com</p>
				</div><!-- /.single-footer-top -->
			</div><!-- /.col-md-4 -->
			<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
				<div class="single-footer-top">
					<p><i class="bitmex-icon-phone-call"></i><span>Call: </span>2800 256 508</p>
				</div><!-- /.single-footer-top -->
			</div><!-- /.col-md-4 -->
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<div class="single-footer-top">
					<p><i class="bitmex-icon-placeholder"></i><span>Address: </span>Suite 20 Bitcoin Street West USA</p>
				</div><!-- /.single-footer-top -->
			</div><!-- /.col-md-4 -->
		</div><!-- /.row -->
	</div><!-- /.thm-container -->
</div><!-- /.footer-top -->

<footer class="site-footer">
	<div class="thm-container">
		<div class="row masonary-layout">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="single-footer-widget">
					<div class="title">
						<h3>About</h3>
					</div><!-- /.title -->
					<ul class="links-list">
						<li><a href="#">Home</a></li>
						<li><a href="#">Jobs</a></li>
						<li><a href="#">Our Team</a></li>
						<li><a href="#">Contact Us</a></li>
						<li><a href="#">Latest News</a></li>
						<li><a href="#">Free Consultancy</a></li>
					</ul><!-- /.links-list -->
				</div><!-- /.single-footer-widget -->
			</div><!-- /.col-md-3 -->
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="single-footer-widget">
					<div class="title">
						<h3>Bitcoin</h3>
					</div><!-- /.title -->
					<ul class="links-list">
						<li><a href="#">Get Started</a></li>
						<li><a href="#">Wallets</a></li>
						<li><a href="#">Buy & Sell Bitcoins</a></li>
						<li><a href="#">Exchange Market</a></li>
						<li><a href="#">Daily Stats</a></li>
					</ul><!-- /.links-list -->
				</div><!-- /.single-footer-widget -->
			</div><!-- /.col-md-3 -->
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="single-footer-widget">
					<div class="title">
						<h3>Legal</h3>
					</div><!-- /.title -->
					<ul class="links-list">
						<li><a href="#">Guide</a></li>
						<li><a href="#">FAQ</a></li>
						<li><a href="#">Advertise</a></li>
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Terms of Use</a></li>
					</ul><!-- /.links-list -->
				</div><!-- /.single-footer-widget -->
			</div><!-- /.col-md-3 -->
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="single-footer-widget">
					<div class="title">
						<h3>Subscribe</h3>
					</div><!-- /.title -->
					<form action="inc/mailchimp/subscribe.php" class="subscribe-form">
						<input type="text" placeholder="Email Address" />
						<button type="submit" class="fa fa-check"></button>
					</form><!-- /.subscribe-form -->
				</div><!-- /.single-footer-widget -->
			</div><!-- /.col-md-3 -->
		</div><!-- /.row -->
	</div><!-- /.thm-container -->
</footer><!-- /.site-footer -->

<div class="footer-bottom">
	<div class="thm-container clearfix">
		<div class="pull-left copy-text">
			<p>&copy; 2020 Yokesen.com All copy rights are reserved.</p>
		</div><!-- /.pull-left copy-text -->
		<div class="social pull-right">
        	<a href="#" class="fa fa-twitter"></a><!--
        	--><a href="#" class="fa fa-facebook"></a><!--
        	--><a href="#" class="fa fa-youtube-play"></a><!--
        	--><a href="#" class="fa fa-pinterest"></a>
        </div><!-- /.social -->
	</div><!-- /.thm-container -->
</div><!-- /.footer-bottom -->