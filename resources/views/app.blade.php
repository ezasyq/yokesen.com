<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Yokesen</title>

    {{-- CSS --}}
    <link rel="stylesheet" href="{{url('/')}}/css/style.css">
    <link rel="stylesheet" href="{{url('/')}}/css/custom.css">
    <link rel="stylesheet" href="{{url('/')}}/css/responsive.css">
</head>
<body class="active-preloader-ovh">

    <div class="preloader"><div class="spinner"></div></div><!-- /.preloader -->
    @include('components.navbar')
    @yield('content')
    @include('components.footer')
    {{-- <h1>Halo Yokesen</h1> --}}


    <div class="scroll-to-top scroll-to-target" data-target="html"><i class="fa fa-angle-up"></i></div>                    

    {{-- JS Files --}}
    <script src="{{url('/')}}/js/jquery.js"></script>
    <script src="{{url('/')}}/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/js/bootstrap-select.min.js"></script>
    <script src="{{url('/')}}/js/jquery.validate.min.js"></script>
    <script src="{{url('/')}}/js/owl.carousel.min.js"></script>
    <script src="{{url('/')}}/js/isotope.js"></script>
    <script src="{{url('/')}}/js/jquery.magnific-popup.min.js"></script>
    <script src="{{url('/')}}/js/waypoints.min.js"></script>
    <script src="{{url('/')}}/js/jquery.counterup.min.js"></script>
    <script src="{{url('/')}}/js/wow.min.js"></script>
    <script src="{{url('/')}}/js/jquery.easing.min.js"></script>
    <script src="{{url('/')}}/js/particles.min.js"></script>
    <script src="{{url('/')}}/js/particles-config.js"></script>
    <script src="{{url('/')}}/js/Chart.min.js"></script>
    <script src="{{url('/')}}/js/chart-config.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="{{url('/')}}/js/custom.js"></script>
</body>
</html>