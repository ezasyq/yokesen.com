@extends('app')

@section('content')

{{-- CAROUSEL --}}
<div id="minimal-bootstrap-carousel" class="carousel slide carousel-fade slider-home-two" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active slide-1" style="background-image: url(img/banner-2-1.jpg);background-position: left center;">
            
            <div class="carousel-caption">
                <div class="thm-container">
                    <div class="box valign-middle">
                        <div class="content ">
                            <h3 data-animation="animated fadeInUp">Yokesen Teknologi <br> Indonesia</h3>
                            <p data-animation="animated fadeInDown">Excepteur sint occaecat cupidatat non proident sunt in culpa <br /> qui officia deserunt mollit anim id est laborum.</p>
                            <a href="#" class="thm-btn yellow-bg" data-animation="animated fadeInDown">Get Started</a>
                        </div>
                    </div>
                </div>
            </div>                
        </div>
        <div class="item slide-2" style="background-image: url(img/banner-2-2.jpg);background-position: left center;">
            
            <div class="carousel-caption">
                <div class="thm-container">
                    <div class="box valign-middle">
                        <div class="content ">
                            <h3 data-animation="animated fadeInUp">Bitcoins are secure <br /> in your pocket</h3>
                            <p data-animation="animated fadeInDown">Excepteur sint occaecat cupidatat non proident sunt in culpa <br /> qui officia deserunt mollit anim id est laborum.</p>
                            <a href="#" class="thm-btn yellow-bg" data-animation="animated fadeInDown">Get Started</a>
                        </div>
                    </div>
                </div>
            </div>                
        </div>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#minimal-bootstrap-carousel" role="button" data-slide="prev">
        <i class="fa fa-angle-left"></i>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#minimal-bootstrap-carousel" role="button" data-slide="next">
        <i class="fa fa-angle-right"></i>
        <span class="sr-only">Next</span>
    </a>

    <ul class="carousel-indicators list-inline custom-navigation">
        <li data-target="#minimal-bootstrap-carousel" data-slide-to="0" class="active"></li><!--
        --><li data-target="#minimal-bootstrap-carousel" data-slide-to="1"></li>
    </ul>
</div>



{{-- APA ITU YOKESEN --}}
<section class="feature-style-one sec-pad">
	<div class="thm-container">
        <p>Apa itu Yokesen?</p>
        <h2>Digital Transformation</h2>
	</div><!-- /.thm-container -->
</section><!-- /.feature-style-one -->



{{-- ELEVATING BRANDS --}}
<section class="count-down-style-one-wrapper sec-pad">
	<div class="thm-container">
        <div class="countdown-title">
            <h3>Elevating Brands <br> through innovation in <br> Digital Transformation.</h3>
        </div><!-- /.countdown-title -->
	</div><!-- /.thm-container -->
</section><!-- /.count-down-style-one-wrapper -->



{{-- APA TARGETMU HARI INI? --}}
<section class="team-style-one sec-pad">
    <div class="thm-container">
        <div class="sec-title">
            <h3>Apa targetmu hari ini?</h3>
        </div><!-- /.sec-title text-center -->
    </div><!-- /.thm-container -->
    <div class="brand-carousel-wrapper">
        {{-- <div class="thm-container"> --}}
            <div class="brand-carousel owl-carousel owl-theme">
                
                <div class="carousel-box">
                    <h4>Digital Marketing and <br> Social Media Advisor</h4>
                    <a href="#">Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Market Growth</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Employee <br> Management System</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Digital Enhancement</h4>
                    <a>Digital Enhancement <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Cost-Competitiveness</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Digital Marketing and <br> Social Media Advisor</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                
    
            </div><!-- /.brand-carousel -->
        {{-- </div><!-- /.thm-container --> --}}
    </div><!-- /.brand-carousel-wrapper -->
</section><!-- /.team-style-one sec-pad -->



{{-- FEATURED STORY --}}
<section class="service-features-style-one sec-pad">
    <div class="thm-container">
        <h1>Featured Story</h1>
        <br>
        <div class="row">
            <div class="col-md-6">
                <div class="single-service-features-style-one">
                    <div class="text-box featured">
                        <p>How to expand <br> business in pandemic</p>
                    </div><!-- /.text-box -->
                </div><!-- /.single-service-features-style-one -->
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <div class="single-service-features-style-one">
                    <div class="text-box featured">
                        <p>How to expand <br> business in pandemic</p>
                    </div><!-- /.text-box -->
                </div><!-- /.single-service-features-style-one -->
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <div class="single-service-features-style-one">
                    <div class="text-box featured">
                        <p>UX is the way to <br> introduce new brand</p>
                    </div><!-- /.text-box -->
                </div><!-- /.single-service-features-style-one -->
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <div class="single-service-features-style-one">
                    <div class="text-box featured">
                        <p>UX is the way to <br> introduce new brand</p>
                    </div><!-- /.text-box -->
                </div><!-- /.single-service-features-style-one -->
            </div><!-- /.col-md-6 -->
             
        </div><!-- /.row -->
    </div><!-- /.thm-container -->
</section><!-- /.service-features-style-one -->
@endsection