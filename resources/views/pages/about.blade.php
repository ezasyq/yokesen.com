@extends('app')

@section('content')

{{-- KAMI MENCIPTAKAN PELUANG --}}
<section class="feature-style-two sec-pad gray-bg">
    <div class="thm-container">
        <h1>Kami menciptakan peluang untuk <br> bisnis anda dapat melebar.</h1>
    </div><!-- /.thm-container -->
</section><!-- /.feature-style-two -->

{{-- APA ITU YOKESEN --}}
<section class="feature-style-one sec-pad">
	<div class="thm-container">
        <p>Apa itu Yokesen?</p>
        <h2>Digital Transformation</h2>
        <p>Yokesen adalah digital company yang mampu untuk membantu bisnis anda dalam hal... Yokesen adalah digital company yang mampu untuk membantu bisnis anda dalam hal... Yokesen adalah digital company yang mampu untuk membantu bisnis anda dalam hal... Yokesen adalah digital company yang mampu untuk membantu bisnis anda dalam hal...</p>
	</div><!-- /.thm-container -->
</section><!-- /.feature-style-one -->

{{-- WE HELP BUSINESS --}}
<section class="feature-style-two sec-pad gray-bg">
    <div class="thm-container">
        <h1>We help business all <br> around the world to grow</h1>
        <marquee behavior="sliding" direction="left">asdasd</marquee>
        {{-- <br><br><br> --}}
    </div><!-- /.thm-container -->
    <section class="btc-static-ticker">
        <div class="btc-static-ticker-slider bx-slider">
            <div class="single-btc-static-ticker slide">
                <img src="img/btc-ticker-1-1.png" alt="" />
                <span class="name">Monero(XMR)</span>
                <span class="amount">$33.892</span>
                <span class="stats"><i class="fa fa-arrow-up"></i>4.8%</span>
            </div><!-- /.slide -->
            <div class="single-btc-static-ticker slide">
                <img src="img/btc-ticker-1-2.png" alt="" />
                <span class="name">Nem(XEM)</span>
                <span class="amount">$1.07588</span>
                <span class="stats"><i class="fa fa-arrow-up"></i>7.22%</span>
            </div><!-- /.slide -->
            <div class="single-btc-static-ticker slide">
                <img src="img/btc-ticker-1-3.png" alt="" />
                <span class="name">Dash(DASH)</span>
                <span class="amount">$857.688</span>
                <span class="stats"><i class="fa fa-arrow-up"></i>9.31%</span>
            </div><!-- /.slide -->
            <div class="single-btc-static-ticker slide">
                <img src="img/btc-ticker-1-4.png" alt="" />
                <span class="name">Bitcoin(BTC)</span>
                <span class="amount">$11759.2</span>
                <span class="stats"><i class="fa fa-arrow-up"></i>5.18%</span>
            </div><!-- /.slide -->
            <div class="single-btc-static-ticker slide">
                <img src="img/btc-ticker-1-5.png" alt="" />
                <span class="name">Ethereum(ETH)</span>
                <span class="amount">$1063.75</span>
                <span class="stats"><i class="fa fa-arrow-up"></i>6.85%</span>
            </div><!-- /.slide -->
            <div class="single-btc-static-ticker slide">
                <img src="img/btc-ticker-1-6.png" alt="" />
                <span class="name">Ripple(XRP) </span>
                <span class="amount">$1.55415</span>
                <span class="stats"><i class="fa fa-arrow-up"></i>4.8%</span>
            </div><!-- /.slide -->
            <div class="single-btc-static-ticker slide">
                <img src="img/btc-ticker-1-7.png" alt="" />
                <span class="name">Litecoin(LTC)</span>
                <span class="amount">$33$195.85</span>
                <span class="stats"><i class="fa fa-arrow-up"></i>7.85%</span>
            </div><!-- /.slide -->
            <div class="single-btc-static-ticker slide">
                <img src="img/btc-ticker-1-1.png" alt="" />
                <span class="name">Monero(XMR)</span>
                <span class="amount">$33.892</span>
                <span class="stats"><i class="fa fa-arrow-up"></i>4.8%</span>
            </div><!-- /.slide -->
            <div class="single-btc-static-ticker slide">
                <img src="img/btc-ticker-1-2.png" alt="" />
                <span class="name">Nem(XEM)</span>
                <span class="amount">$1.07588</span>
                <span class="stats"><i class="fa fa-arrow-up"></i>7.22%</span>
            </div><!-- /.slide -->
            <div class="single-btc-static-ticker slide">
                <img src="img/btc-ticker-1-3.png" alt="" />
                <span class="name">Dash(DASH)</span>
                <span class="amount">$857.688</span>
                <span class="stats"><i class="fa fa-arrow-up"></i>9.31%</span>
            </div><!-- /.slide -->
            <div class="single-btc-static-ticker slide">
                <img src="img/btc-ticker-1-4.png" alt="" />
                <span class="name">Bitcoin(BTC)</span>
                <span class="amount">$11759.2</span>
                <span class="stats"><i class="fa fa-arrow-up"></i>5.18%</span>
            </div><!-- /.slide -->
            <div class="single-btc-static-ticker slide">
                <img src="img/btc-ticker-1-5.png" alt="" />
                <span class="name">Ethereum(ETH)</span>
                <span class="amount">$1063.75</span>
                <span class="stats"><i class="fa fa-arrow-up"></i>6.85%</span>
            </div><!-- /.slide -->
            <div class="single-btc-static-ticker slide">
                <img src="img/btc-ticker-1-6.png" alt="" />
                <span class="name">Ripple(XRP) </span>
                <span class="amount">$1.55415</span>
                <span class="stats"><i class="fa fa-arrow-up"></i>4.8%</span>
            </div><!-- /.slide -->
            <div class="single-btc-static-ticker slide">
                <img src="img/btc-ticker-1-7.png" alt="" />
                <span class="name">Litecoin(LTC)</span>
                <span class="amount">$33$195.85</span>
                <span class="stats"><i class="fa fa-arrow-up"></i>7.85%</span>
            </div><!-- /.slide -->
        </div><!-- /.btc-static-ticker-slider -->
    </section><!-- /.btc-static-ticker -->
</section><!-- /.feature-style-two -->

{{-- BOARD OF DIRECTOR --}}
<section class="team-style-one sec-pad">
    <div class="thm-container">
        <div class="sec-title">
            <h3 style="font-style: italic">Board of Director</h3>
        </div><!-- /.sec-title text-center -->
    </div><!-- /.thm-container -->
    <div class="brand-carousel-wrapper">
        {{-- <div class="thm-container"> --}}
            <div class="brand-carousel owl-carousel owl-theme">
                
                <div class="carousel-box">
                    <h4>Digital Marketing and <br> Social Media Advisor</h4>
                    <a href="#">Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Market Growth</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Employee <br> Management System</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Digital Enhancement</h4>
                    <a>Digital Enhancement <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Cost-Competitiveness</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Digital Marketing and <br> Social Media Advisor</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                
    
            </div><!-- /.brand-carousel -->
        {{-- </div><!-- /.thm-container --> --}}
    </div><!-- /.brand-carousel-wrapper -->
</section><!-- /.team-style-one sec-pad -->

{{-- MARKETPLACE DIVISION --}}
<section class="team-style-one sec-pad">
    <div class="thm-container">
        <div class="sec-title">
            <h3 style="font-style: italic">Marketplace Division</h3>
        </div><!-- /.sec-title text-center -->
    </div><!-- /.thm-container -->
    <div class="brand-carousel-wrapper">
        {{-- <div class="thm-container"> --}}
            <div class="brand-carousel owl-carousel owl-theme">
                
                <div class="carousel-box">
                    <h4>Digital Marketing and <br> Social Media Advisor</h4>
                    <a href="#">Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Market Growth</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Employee <br> Management System</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Digital Enhancement</h4>
                    <a>Digital Enhancement <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Cost-Competitiveness</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Digital Marketing and <br> Social Media Advisor</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                
    
            </div><!-- /.brand-carousel -->
        {{-- </div><!-- /.thm-container --> --}}
    </div><!-- /.brand-carousel-wrapper -->
</section><!-- /.team-style-one sec-pad -->


{{-- COPYWRITING DIVISION --}}
<section class="team-style-one sec-pad">
    <div class="thm-container">
        <div class="sec-title">
            <h3 style="font-style: italic">Copywriting Division</h3>
        </div><!-- /.sec-title text-center -->
    </div><!-- /.thm-container -->
    <div class="brand-carousel-wrapper">
        {{-- <div class="thm-container"> --}}
            <div class="brand-carousel owl-carousel owl-theme">
                
                <div class="carousel-box">
                    <h4>Digital Marketing and <br> Social Media Advisor</h4>
                    <a href="#">Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Market Growth</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Employee <br> Management System</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Digital Enhancement</h4>
                    <a>Digital Enhancement <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Cost-Competitiveness</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="carousel-box">
                    <h4>Digital Marketing and <br> Social Media Advisor</h4>
                    <a>Digital Marketing <i class="fa fa-arrow-right"></i></a>
                    <br>
                    <a>Social Media <i class="fa fa-arrow-right"></i></a>
                </div>
                
    
            </div><!-- /.brand-carousel -->
        {{-- </div><!-- /.thm-container --> --}}
    </div><!-- /.brand-carousel-wrapper -->
</section><!-- /.team-style-one sec-pad -->

@endsection